package pl.sda.services;

import org.springframework.validation.BindingResult;
import pl.sda.dto.UserCreateForm;
import pl.sda.dto.UsersDto;
import pl.sda.entities.User;

import java.util.List;

public interface UserService {

    User findUserById(Integer id);

    User findUserByEmail(String email);

    BindingResult validateUser(UserCreateForm user, BindingResult bindingResult);

    //void saveUser(User user);
    void saveUser(UserCreateForm userForm);

    void deleteUser(String email);

    List<UsersDto> getUserList();

    void addAdminRole(Integer id);

}