package pl.sda.services;

import javax.servlet.http.HttpServletRequest;

public interface GeoLocationService {

    String getCityNameFromRequest(HttpServletRequest request);

}
