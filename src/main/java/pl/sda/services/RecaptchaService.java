package pl.sda.services;

public interface RecaptchaService {
    boolean isResponseValid(String remoteIp, String response);
}
