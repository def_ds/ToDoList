package pl.sda.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.dto.ForecastDto;
import pl.sda.utils.OWMConnector;

import java.io.IOException;

@Service
public class ForecastServiceImpl implements ForecastService {

    @Autowired
    private OWMConnector owmConnector;
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public ForecastDto getForecastDto(String cityName) {
        String forecastJson = this.getForecast(cityName);
        ForecastDto forecastDto = this.jsonToForecastDto(forecastJson);
        return forecastDto;
    }

    private String getForecast(String cityName) {
        String JsonFromServer = owmConnector.getDataFromOWMServer(cityName);
        return JsonFromServer;
    }

    private ForecastDto jsonToForecastDto(String json) {
        ForecastDto forecastDto = new ForecastDto();
        try {
            JsonNode root = objectMapper.readTree(json);
            forecastDto.setName(root.get("name").asText());
            forecastDto.setTemp(root.get("main").get("temp").asDouble());
            forecastDto.setHumidity(root.get("main").get("humidity").asDouble());
            forecastDto.setSpeed(root.get("wind").get("speed").asDouble());
            forecastDto.setPressure(root.get("main").get("pressure").asDouble());
            forecastDto.setIcon(root.get("weather").findValue("icon").asText("03d")+".png");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return forecastDto;
    }
}
