package pl.sda.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.services.SecurityService;

@Controller
public class LoginController {

    @Autowired
    private SecurityService securityService;

    @GetMapping("/login")
    public ModelAndView showLoginPage(Model map) {
        return new ModelAndView("login");
    }


    @RequestMapping({"/", "/user/welcome"})
    public ModelAndView showWelcomePage(ModelMap map) {
        String username = securityService.findLoggedInName();
        map.addAttribute("username", username);
        return new ModelAndView("/user/welcome", map);
    }
}
