package pl.sda.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.ForecastDto;
import pl.sda.services.ForecastService;
import pl.sda.services.GeoLocationService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ForecastController {

    @Autowired
    ForecastService forecastService;
    @Autowired
    private GeoLocationService geoLocationService;

    @GetMapping("/user/forecast")
    public ModelAndView showForecastPage(ModelMap map, HttpServletRequest request) {
        String cityName = geoLocationService.getCityNameFromRequest(request);
        ForecastDto forecastDto = forecastService.getForecastDto(cityName);
        map.addAttribute("forecastDto", forecastDto);
        return new ModelAndView("/user/forecast", map);
    }
}
