package pl.sda.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccessDenied {

    @GetMapping("/access-denied")
    public ModelAndView showLoginPage(Model map) {
        return new ModelAndView("access-denied");
    }


}
