package pl.sda.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.UserCreateForm;
import pl.sda.services.NotyficationService;
import pl.sda.services.SecurityService;
import pl.sda.services.UserService;
import pl.sda.validator.RecaptchaFormValidator;

import javax.validation.Valid;

@Controller
public class UserCreateController {

    private UserService userService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private NotyficationService notyficationService;


    private final RecaptchaFormValidator recaptchaFormValidator;

    @Autowired
    public UserCreateController(UserService userService,
                                RecaptchaFormValidator recaptchaFormValidator) {
        this.userService = userService;
        this.recaptchaFormValidator = recaptchaFormValidator;
    }


    @InitBinder("userForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(recaptchaFormValidator);
    }


    @ModelAttribute("recaptchaSiteKey")
    public String getRecaptchaSiteKey(@Value("${recaptcha.site-key}") String recaptchaSiteKey) {
        return recaptchaSiteKey;
    }


    @GetMapping("/registration")
    public ModelAndView showSingUpPage(ModelMap modelMap) {
        modelMap.addAttribute("userForm", new UserCreateForm());
        return new ModelAndView("registration", modelMap);
    }

    @PostMapping("/registration")
    public String postSingUpPage(@ModelAttribute("userForm") @Valid UserCreateForm userForm, BindingResult bindingResult) {
        bindingResult = userService.validateUser(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        try{
            notyficationService.sendNotyfication(userForm);
        }catch (MailException e){
            System.out.println("Błąd w wysyłaniu wiadomości mail");
        }
        userService.saveUser(userForm);
        securityService.autoLogin(userForm.getEmail(), userForm.getConfirmPassword());
        return "redirect:/user/welcome";
    }
}
