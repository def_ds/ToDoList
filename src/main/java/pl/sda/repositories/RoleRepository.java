package pl.sda.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
