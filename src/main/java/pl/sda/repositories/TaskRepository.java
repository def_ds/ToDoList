package pl.sda.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.entities.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    List<Task> findAllByUser_Email(String email);

    Task findByIdAndUser_Email(Integer id, String email);
}
