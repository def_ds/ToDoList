package pl.sda.configurations;

import com.maxmind.db.Reader;
import com.maxmind.geoip2.DatabaseReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Bean
    public SimpleDateFormat simpleDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf;
    }

    @Bean
    public DatabaseReader databaseReader() {
        DatabaseReader reader = null;
        try {
            Resource resource = resourceLoader.getResource("classpath:static/db/GeoLite2-City.mmdb");
            InputStream inputStream = resource.getInputStream();
            reader = new DatabaseReader.Builder(inputStream).fileMode(Reader.FileMode.MEMORY).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return reader;
    }
}