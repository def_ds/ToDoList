package pl.sda.utils;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import pl.sda.entities.Task;
import pl.sda.entities.User;

import java.io.*;
import java.util.List;

@Component
public class XLSexport {

    public File exportXLS(User user) throws IOException {
        File file = new File(user.getEmail() + ".xls");
        List<Task> taskList = user.getTask();
        try (InputStream is = XLSexport.class.getResourceAsStream("tasks_template.xls")) {
            try (OutputStream os = new FileOutputStream(file)) {
                Context context = new Context();
                context.putVar("taskList", taskList);
                JxlsHelper.getInstance().processTemplate(is, os, context);
            }
        }
        return file;
    }
}
