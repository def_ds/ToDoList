package pl.sda.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Component
public class OWMConnector {
    private URL url;
    private HttpURLConnection connection;
    @Value("${owm.appid}")
    private String appid;
    @Value("${owm.url}")
    private String owmUrl;
    @Value("${owm.units}")
    private String owmUnits;

    public String getDataFromOWMServer(String cityName) {
        try {
            this.setUrl(cityName);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int responseCode = connection.getResponseCode();

            if (responseCode == 200) {
                String responseFromOWM = this.getResponseFromConnection(connection);
                return responseFromOWM;
            } else {
                throw new RuntimeException("Server error code: " + responseCode);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    private void setUrl(String cityName) {
        try {
            this.url = new URL(this.owmUrl + cityName + this.appid + this.owmUnits);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private String getResponseFromConnection(HttpURLConnection connection) throws IOException {
        StringBuffer sb = new StringBuffer();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
        }
        return sb.toString();
    }
}

